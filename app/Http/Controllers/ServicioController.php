<?php

namespace FindService\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use FindService\Http\Requests;
use FindService\Servicio;
use FindService\Categoria;
use FindService\Http\Requests\ServicioFormRequest;
use DB;


class ServicioController extends Controller
{

  public function index(){

  }

  public function create(){
    return view('signUpService');
  }

  public function show($id){
    $servicio = Servicio::findOrFail($id);
    return view('viewService')->with('servicio',$servicio);
  }

  public function edit($id){
    $servicio = Servicio::findOrFail($id);
    return view('signUpService')->with('servicio',$servicio);
  }

  public function update($id){
    $servicio = Servicio::findOrFail($id);

    $servicio->nombre=$request->get('nombreServicio');
    $servicio->logo=$request->get('logoServicio');
    $servicio->direccion=$request->get('direccion');
    $servicio->sucursales=$request->get ('sucursalesServicio');
    $servicio->horaApertura=$request->get('horarioApertura');
    $servicio->horaCierre=$request->get('horarioCierre');
    $servicio->telefono=$request->get('numtelefono');
    $servicio->celular=$request->get('celularServicio');
    $servicio->descripcion=$request->get('descripcionServicio');
    $servicio->fotografias=$request->get('fotografiasServicio');
    $servicio->nombreContacto=$request->get('nombrencargado');
    $servicio->correoContacto=$request->get('correoContacto');
    $servicio->puntuacion=0;
    $servicio->urlOficial=$request->get('urlServicio');
    $servicio->idCategoria=2;
    $servicio->save();
    return redirect()->route('home');
  }

  public function destroy(){

  }

  public function store(ServicioFormRequest $request)
  {
    $servicio = new Servicio;
    $servicio->nombre=$request->get('nombreServicio');
    if ($request->hasFile('logoServicio')) {
      $servicio->logo=$request->file('logoServicio')->store('public');
    }
    $servicio->direccion=$request->get('direccion');
    $servicio->sucursales=$request->get ('sucursalesServicio');
    $servicio->horaApertura=$request->get('horarioApertura');
    $servicio->horaCierre=$request->get('horarioCierre');
    $servicio->telefono=$request->get('numtelefono');
    $servicio->celular=$request->get('numcelular');
    $servicio->descripcion=$request->get('descripcionServicio');
    $servicio->fotografias=$request->get('fotografiasServicio');
    $servicio->nombreContacto=$request->get('nombrencargado');
    $servicio->correoContacto=$request->get('correoContacto');
    $servicio->puntuacion=0;
    $servicio->urlOficial=$request->get('urlServicio');
    $servicio->idCategoria=$request->get('CategoriaServicio');
    $servicio->save();
    //dd($request->file('logo'));
    return redirect()->route('home');
  }

  public function registrarServicio(){
    return view('signUpService');
  }

  public function servicio(){
    return view('Servicio');
  }

  public function categoria($idCategoria){
    $cat = Categoria::findOrFail($idCategoria);
    $servicios = Servicio::where('idCategoria',$idCategoria);
    return view('categoria')->with('cat',$cat)->with('servicios',$servicios);
  }

  public function categoriaSalud(){
    $servicio = Servicio::get();
    return view('plantillaCategoria')->with('servicio',$servicio);
  }

  public function categoriaComida(){
    return view('comida');
  }
}
