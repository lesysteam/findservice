<?php

namespace FindService;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table='servicio';
    protected $primaryKey='idservicio';
    public $timestamps=false;

    protected $filable=[
      'nombre',
      'logo',
      'direccion',
      'sucursales',
      'horaApertura',
      'horaCierre',
      'telefono',
      'celular',
      'descripcion',
      'fotografias',
      'nombreContacto',
      'correoContacto',
      'correoContacto',
      'puntuacion',
      'urlOficial',
      'idcategoria',
    ];

    protected $guarded=[

    ];
}
