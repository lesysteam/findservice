<?php

namespace FindService;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
  protected $table='categoria';
  protected $primaryKey='idcategoria';
  public $timestamps=false;

  protected $filable=[
  ];

  protected $guarded=[
    'nombre',
  ];
}
