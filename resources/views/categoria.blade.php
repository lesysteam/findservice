@extends ('plantilla')

@section ('busqueda')

    <!-- Contenido principal de la pagina -->
    <br><br>
    <div class="container">
      <div class="row">
        <section class="col s12 center">
          <h1 class = "section white-text" style="background: #f12d2d;">{{ $cat->nombre }}</h1>
        </section>
        <section class="col s8 offset-s2 center">
          <form>
            <div class="input-field">
              <input id="search_bar" type="search" required>
              <label class="" for="search_bar"><i class="material-icons left">search</i>Busca servicios como hospitales, restaurantes, tiendas, etc...</label>
              <i class="material-icons">close</i>
            </div>
          </form>
        </section>
      </div>
    </div>

    <div class="container">
      <div class="row">
        @foreach ($servicios as $service)
          <div class="col s6 m4 l3">
            <div class="card">
              <div class="card-image">
                <!--img src="imagenes\pruebaServicio\logo.png"-->
                <a href="{{ route('servicio.show',$service->idservicio) }}" class="btn-floating pulse halfway-fab waves-effect waves-light" style="background: #f12d2d;"><i class="material-icons">subdirectory_arrow_right</i></a>
              </div>
              <div class="card-content">
                <strong style="font-weight: bold" class="blue-text text-darken-4">{{ $service->nombre }}<strong>
                </div>
              </div>
            </div>
        @endforeach
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.parallax');
      var instances = M.Parallax.init(elems);
    });
    </script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.carousel');
      var instances = M.Carousel.init(elems);
    });
    </script>
@endsection
