@extends('plantilla')
@section('contenido')
    <div class="container">
      <div class="row">
        <div class="col s6 l7">
          <br><br><br>
          <h4 class="center-align">Inicio de Sesión</h4><br>
          <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
              @csrf

              <div class="form-group row">
                <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Correo Electronico') }}</label>

                <div class="col-md-6">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                  @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                <div class="col-md-6">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!--div class="form-group row">
                <div class="col-md-6 offset-md-4">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                      {{ __('Remember Me') }}
                    </label>
                  </div>
                </div>
              </div-->

              <div class="center-align form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                  <button type="submit" class="green btn btn-primary">
                    Iniciar Sesión
                  </button>

                  <a class="green btn btn-link" href="{{ route('password.request') }}">
                    ¿Olvidaste tu contraseña?
                  </a>
                </div>
              </div>

            </form>
          </div>

        </div>

        <!-- Cuadro de opciones de inicio de sesión -->
        <div class="col s6 l4 offset-l1">
          <br><br><br>
          <div class="card" id="login_options">
            <div class="card-content center">
              <span class="card-title">Opciones de Inicio de Sesión</span>
            </div>
            <div class="card-action center">
              <a href="{{url('/auth/google')}}" id="google" class="btn waves-effect waves-red"><img class="left" src="imagenes/Google.png" alt="Google" height="30px">Google</a>
              <a href="{{url('/auth/facebook')}}" id="facebook" class="btn waves-effect waves-red"><img class="left" src="imagenes/Facebook.png" alt="Facebook" height="30px">Facebook</a>
            </div>
          </div>
          <br>
          <!-- Cuadro de enlace a registro -->
          <div class="card" id="sign_up">
            <div class="card-content center">
              <span class="card-title">¿Aún no estás registrado?</span>
            </div>
            <div class="card-action center">
              <a href="{{ route('signup') }}" class="btn green waves-effect waves-light" style="font-weight: bold;">Registrarme</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
@endsection
