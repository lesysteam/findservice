@extends('plantilla')
@section('contenido')
    <!-- Contenido principal de la pagina -->
    &nbsp;
    <div class="container">
      <br>
      <div class="row">
          <div class="col m8 offset-m2">
              <div class="card">
                  <div class="card-title center "><h4>{{ __('Restablecer Contraseña') }}</h4></div>

                  <div class="card-content">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif

                      <form method="POST" action="{{ route('password.email') }}">
                          @csrf

                          <div class="input-field row">
                              <label for="email" >{{ __('Correo Electronico') }}</label>

                              <div class="col s12">
                                  <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="input-field row">
                              <div class="col m6 offset-m3">
                                  <button type="submit" class="btn green btn-primary">
                                      {{ __('Enviar enlace para Restablecer Constraseña') }}
                                  </button>
                              </div>
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
@endsection
