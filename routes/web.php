.<?php

use FindService\Categoria;
use FindService\Servicio;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Rutas de Sesión
Route::get('/','PagesController@inicio')->name('home');
Route::get('login','PagesController@login')->name('login');
Route::get('signup','PagesController@signup')->name('signup');
Route::post('login2','Auth\LoginController@login2')->name('login2');
Auth::routes();
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::get('perfil/{id}','PagesController@verPerfil')->name('perfil');
Route::post('recuperarContrasena','Auth\ForgotPasswordController@recuperarContrasena')->name('recuperarContra');
Route::get('recuperarPassword', function() {
    return view('auth/recuperarContra');
})->name('password.request');

//Rutas de Inicio de Sesión con Google o Facebook
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

//Rutas de Servicios
Route::get('Servicio', 'ServicioController@show')->name('showService');
Route::get('signService', 'ServicioController@create')->name('sservice');
Route::get('service', 'ServicioController@servicio')->name('service');
Route::get('catSalud','ServicioController@categoriaSalud')->name('cat_salud');

//Rutas del blog
Route::get('blog', 'PagesController@blog')->name('blog');
//Route::controller('post', 'PostController');
//Route::controller('comments', 'CommentController');

Route::get('categoria/{id}', function($idCategoria){
  $cat = Categoria::findOrFail($idCategoria);
  $servicios = Servicio::where('idCategoria',$idCategoria)->get();
  return view('categoria')->with('cat',$cat)->with('servicios',$servicios);
})->name('categoria');

Route::resource('prueba','PagesController');
Route::resource('servicio','ServicioController');
