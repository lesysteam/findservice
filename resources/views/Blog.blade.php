@extends ('plantilla')

@section ('contenido')
  <!-- Contenido principal de la pagina -->
  <br><br>
  <div class="container">
    <div class="row">
      <section class="col s8 offset-s2 center">
        <!-- -->
        <ul class="collapsible">
          <li>
            <div class="collapsible-header">
              <i class="material-icons">send</i>Agregar Post
            </div>
            <div class="collapsible-body">
              <div class="modal-content">
                <div class="row">
                  <form class="col s12">
                    <div class="row">
                      <div class="input-field col s12">
                        <strong style="font-weight: bold" class="blue-text text-darken-4">Título: <strong>
                            <input id="input_text" type="text" data-length="50">
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <strong style="font-weight: bold" class="blue-text text-darken-4">Comentario: <strong>
                            <textarea id="textarea2" class="materialize-textarea" data-length="200"></textarea>
                      </div>
                    </div>
                    <div class="row">
                        <form action="#">
                            <div class="file-field input-field">
                              <div class="btn">
                                <span>File</span>
                                <input type="file">
                              </div>
                              <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                              </div>
                            </div>
                          </form>
                    </div>
                  </form>
                </div>
                <div>
                </div>

                <div class="row">
                  <div class="col s12">
                    <button type="button" class="btn blue darken-4" name="button">Enviar Comentario</button>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
        <!--Card de visualizacion-->
        <div class="card">
          <div class="card-image">
            <img class="activator" src="https://www.estrategiaynegocios.net/csp/mediapool/sites/dt.common.streams.StreamServer.cls?STREAMOID=yoKSb9IAT3noUva4Uy5vVM$daE2N3K4ZzOUsqbU5sYtNU7N1fD39KajhHfIQXYMg6FB40xiOfUoExWL3M40tfzssyZqpeG_J0TFo7ZhRaDiHC9oxmioMlYVJD0A$3RbIiibgT65kY_CSDiCiUzvHvODrHApbd6ry6YGl5GGOZrs-&CONTENTTYPE=image/jpeg">
          </div>
          <div class="card-content blue-text text-darken-4">
            <strong style="font-weight: bold" class="card-title">Pollo Campero</strong>
            <p>Aqui va el comentario</p>
          </div>
        </div>
      </section>
    </div>
  </div>
  <!--  -->
  @endsection
  @section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems);
    });
  </script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('.parallax');
      var instances = M.Parallax.init(elems);
    });
  </script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('.carousel');
      var instances = M.Carousel.init(elems);
    });
  </script>
  <script>
    var instance = M.Carousel.init({
      fullWidth: true,
      indicators: true
    });
  </script>
  <script>
  document.addEventListener('DOMContentLoaded', function () {
    M.AutoInit();
  });
</script>
@endsection
