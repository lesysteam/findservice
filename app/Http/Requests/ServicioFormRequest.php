<?php

namespace FindService\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicioFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'nombreServicio' => 'required|max:50',
          'direccion' => 'required|max:100',
          'horarioapertura' => '',
          'horariocierre' => '',
          'numtelefono' => 'required|max:13',
          'nombrencargado' => 'required|max:50'
        ];
    }
}
