@extends('plantilla')
@section('contenido')
    <!-- Contenido principal de la pagina -->
    &nbsp;
    <div class="container">
      <br>
        <div class="row">
            <div class="col m8 offset-m2">
                <div class="card">
                    <div class="card-title center"><h4>{{ __('Restablecer Contraseña') }}</h4></div>

                    <div class="card-content">
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="row">
                                <label for="email" class="col s3  ">Correo Electronico</label>

                                <div class="input-field col s12">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="password" class="col s3">{{ __('Contraseña') }}</label>

                                <div class="input-field col s12">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="password-confirm" class="col s3">{{ __('Confirmar Contraseña') }}</label>

                                <div class="input-field col s12">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s8 m6 offset-m4 offset-s2">
                                    <button type="submit" class="green btn btn-primary">
                                        {{ __('Restablecer Contraseña') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
@endsection
