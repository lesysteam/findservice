<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
    <link rel="stylesheet" href="/css/styles.css">
    <title>Find Service Xela</title>
    <link rel="shortcut icon" href="/imagenes/l2.ico"/>
    @yield('head')
  </head>

  <body>
    <header>
      <!-- Barra de navegación responsive-->
      <div id="barra-nav" class="navbar-fixed">
        <nav>
          <div id="div_header" class="nav-wrapper container">
            <!-- Logo -->
            <a href="{{ route('home') }}" class="brand-logo"><img src="/imagenes/l2.png" alt="Logo" class="left hide-on-med-and-down" height="75px">Find Service</a>
            <!-- Icono de menu (responsive) -->
            <a href="#" data-target="menu-movil" id="menu_res" class="sidenav-trigger light"><i class="material-icons">menu</i></a>
            <!-- Opciones de la barra de navegacion normal -->
            <ul id="menu" class="right hide-on-med-and-down">
              <li><a href=" {{ route('blog') }}" id="nav_btn" class="waves-effect waves-light">Blog</a></li>
              <li>
                <a href="#" id="nav_categorias" class="dropdown-trigger btn-large waves-effect waves-red" data-target='cat_dropdown'>
                  <i class="large material-icons right">expand_more</i>Categorías
                </a>
              </li>
              <ul id='cat_dropdown' class='dropdown-content'>
                <li><a id="categoria" href="{{ route('categoria','1' ) }}">Salud</a></li>
                <li><a id="categoria" href="{{ route('categoria','2' ) }}">Comida</a></li>
                <li><a id="categoria" href="{{ route('categoria','3' ) }}">Comercio</a></li>
                <li><a id="categoria" href="{{ route('categoria','4' ) }}">Profesional</a></li>
                <li><a id="categoria" href="{{ route('categoria','5' ) }}">Ocio</a></li>
              </ul>
              @guest
                <a href="#" id="nav_login" class="dropdown-trigger btn-large waves-effect waves-red" data-target='nav_dropdown'>
                  <i class="large material-icons right">expand_more</i>Sesión
                </a>
                <ul id='nav_dropdown' class='dropdown-content'>
                  <li><a id="sesion_dropdown" href="{{ route('login') }}">Iniciar Sesión</a></li>
                  <li><a id="sesion_dropdown" href="{{ route('signup') }}">Crear Usuario</a></li>
                  <li><a id="sesion_dropdown" href="{{ route('servicio.create') }}">Registrar Servicio</a></li>
                </ul>
              @else
                <a href="#" id="nav_logout" class="center dropdown-trigger btn-large waves-effect waves-red" data-target='nav_dropdown'>
                  <i class="large material-icons right">expand_more</i>{{ Auth::user()->name }}
                </a>
                <ul id='nav_dropdown' class='dropdown-content'>
                  <li><a id="sesion_dropdown" href="{{ route('sservice') }}">Registrar Servicio</a></li>
                  <li><a id="sesion_dropdown" href="{{ route('login') }}">Ver Perfil</a></li>
                  <li>
                    <a id="sesion_dropdown" href="{{ route('perfil',Auth::user()->id) }}">Ver Perfil</a>
                  </li>
                  <li>
                    <a id="sesion_dropdown" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                     Cerrar Sesión
                     </a>
                   </li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              @endguest
            </ul>
          </div>
        </nav>
      </div>

      <!-- Opciones del menu lateral (responsive) -->
      <ul class="sidenav" id="menu-movil">
        <li> <br> </li>
        <li class="center">
          <span id="logo_menu">
            <a href="{{ route('home') }}"><img src="imagenes/l4.png" alt="Logo" height="100px"></a>
          </span>
        </li>

        <li> <hr> </li>

        @guest
          <a href="#" id="snav_login" class="center-align dropdown-trigger btn-large waves-effect waves-red" data-target='snav_dropdown'>
            <i class="large material-icons right">expand_more</i>Sesión
          </a>
          <ul id='snav_dropdown' class='dropdown-content'>
            <li><a id="sesion_dropdown" href="{{ route('login') }}">Iniciar Sesión</a></li>
            <li><a id="sesion_dropdown" href="{{ route('signup') }}">Crear Usuario</a></li>
            <li><a id="sesion_dropdown" href="{{ route('sservice') }}">Registrar Servicio</a></li>
          </ul>
        @else
          <li>
            <div class="user-view">
              <!--div class="background">
                <img src="imagenes/Comida.jpg">
              </div-->
              <a href="#"><img class="circle center" src="" alt="Foto" height="200px"></a>
              <a href="#"><span class="black-text name" style="font-size: large;">{{ Auth::user()->name }}</span></a>
              <a href="#"><span class="black-text name" style="font-size: medium;">{{ Auth::user()->email }}</span></a>
              <a href="#" id="snav_logout" class="dropdown-trigger btn waves-effect waves-red" data-target='snav_dropdown'>
                <i class="material-icons right">expand_more</i>{{ Auth::user()->nombre }} {{ Auth::user()->apellido }}

              </a>
              <ul id='snav_dropdown' class='dropdown-content'>
                <li><a id="sesion_dropdown" href="">Ver Perfil</a></li>
                <li>
                  <a id="sesion_dropdown" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                    Cerrar Sesión
                  </a>
                </li>
              </ul>
            </div>
          </li>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        @endguest

        <li><a href="#" id="snav_btn" class="waves-effect waves-light">Servicios</a></li>
        <li><a href="#" id="snav_btn" class="waves-effect waves-light">Historial</a></li>
        <li><a href="#" id="snav_btn" class="waves-effect waves-light">Blog</a></li>
      </ul>
    </header>


    <!-- Contenido principal de la pagina -->
    <br>

    @yield('busqueda')

    <br><br>

    @yield('contenido')


    <footer class="page-footer">
      <div class="container">
        <div class="row">
          <div class="col s6">
            <h5 class="white-text">Find Service</h5>
            <p class="grey-text text-lighten-4">Busca cualquier servicio, Aquí lo encontrarás!</p>
          </div>
        </div>
      </div>
      <div class="footer-copyright" >
        <div class="container white-text">
          2018 Esta página está inspirada en
          <a id="findoctor" class="btn" href="https://www.findoctor.com/" target="_blank">Findoctor.com ©</a>
        </div>
      </div>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });

      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.parallax');
        var instances = M.Parallax.init(elems);
      });

      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.slider');
        var instances = M.Slider.init(elems,{
          interval:5000
        });
      });

      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.dropdown-trigger');
        var instances = M.Dropdown.init(elems);
      });
    </script>
    @yield('scripts')
  </body>
</html>
