@extends('plantilla')
@section('contenido')
  <div class="container">
    <div class="row">
      <br><br><br><br>
      <h4 class="center-align">Creación de Usuario</h4>
      <br>
      <!--<form id="singup_form" class="col s6 l7">-->
        @if (count($errors)>0)
        <div class="">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
        @endif

        <div class="col s8 offset-s2">
          {!!Form::open(array('url'=>'prueba','method'=>'POST','autocomplete'=>'off'))!!}
          {{Form::token()}}
          <!--Nombre y Apellido-->
          <div class="row">
            <div class="input-field col s12 l6">
              <input id="sign_up_name" name="nombre" type="text" class="validate" required="required">
              <label for="sign_up_name">Nombre</label>
            </div>
            <div class="input-field col s12 l6">
              <input id="sign_up_last_name" name="apellido" type="text" class="validate" required="required">
              <label for="sign_up_last_name">Apellido</label>
            </div>
          </div>
          <!--Nombre de Usuario-->
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">person</i>
              <input id="sign_up_nickname" name="nombre_usuario" type="text" class="validate" >
              <label for="sign_up_nickname">Nombre de Usuario</label>
            </div>
          </div>
          <!--Correo-->
          <div class="row">
            <div class="input-field col s12 m7">
              <i class="material-icons prefix">alternate_email</i>
              <input id="sign_up_email" name="correo" type="text" class="validate" required="required">
              <label for="sign_up_email">Correo</label>
            </div>
            <div class="input-field col s12 m5">
              <i class="material-icons prefix">phone</i>
              <input id="sign_up_phone" name="numero_telefono" type="text" class="validate">
              <label for="sign_up_phone">Numero de Teléfono</label>
            </div>
          </div>
          <!--Contraseña y confirmacion-->
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">vpn_key</i>
              <input id="sign_up_password" name="contraseña" type="password" class="validate" required="required">
              <label for="sign_up_password">Contraseña</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">vpn_key</i>
              <input id="sign_up_confirm_password" type="password" class="validate" required="required">
              <label for="sign_up_confirm_password">Confirma tu Contraseña</label>
            </div>
          </div>
          <!--Fecha de nacimiento-->
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">date_range</i>
              <input id="sign_up_birthday" name="fecha_nacimiento" type="text" class="datepicker">
              <label for="sign_up_birthday">Fecha de Nacimiento</label>
            </div>
            <script>
              document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('.datepicker');
                var instances = M.Datepicker.init(elems, {
                  autoClose: true,
                  format: 'yyyy-mm-dd',
                  yearRange: 2
                });
              });
            </script>
          </div>
          <!--Boton de registro-->
          <div class="row">
            <div class="col s6 offset-s3 offset-l4">
              <button id="botonsignup" type="submit" class="btn-large green" name="button">Registrarme</button>
              <script src="js/push.min.js"></script>
              <script>
                var btn = document.getElementById('botonsignup');
                btn.addEventListener('click',function(){
                  Push.create("FindService.com", {
                      body: "Se ha registrado un nuevo usuario al servicio FindService",
                      icon: 'imagenes/l3.png',
                      timeout: 4000,
                      onClick: function () {
                          window.focus();
                          this.close();
                      }
                  });
                });
              </script>
            </div>
          </div>
          {!!Form::close()!!}
          <!--</form>-->
        </div>
    </div>
  </div>
@endsection
