@extends ('plantilla')
@section('head')
    <link rel="stylesheet" href="css/styles.css">
@endsection
@section ('contenido')
  <!-- Contenido principal de la pagina -->
  <br>
  <div class="container">
    <div class="row">
      <div class="col s1"></div>
      <div class="col s12 m8 offset-s1 l6 offset-l3">
        <div class="col s5">
          <img src="imagenes\pruebaServicio\fotoPerfil.jpg" alt="" class="circle responsive-img">
        </div>
        <div class="col s12 blue-text text-darken-4">
          <h3 style="font-weight: bold">Hugo Lucas</h3>
          <div class="row">
            <form class="col s6 l7">
            </form>
          </div>
        </div>
      </div>
      <!--Correo-->
      <div class="row">
          <div class="col s3"><h1></h1></div>
          <div class="input-field col s12 m5">
              <i class="material-icons prefix">person_pin</i>
              <input id="" type="text" class="validate">
              <label for="">Nombre de Usuario</label>
          </div>
        </div>
      <!--Correo-->
      <div class="row">
          <div class="col s3"><h1></h1></div>
          <div class="input-field col s12 m5">
              <i class="material-icons prefix">alternate_email</i>
              <input id="" type="text" class="validate">
              <label for="">Correo</label>
          </div>
        </div>
      <!--Correo-->
      <div class="row">
          <div class="col s3"><h1></h1></div>
          <div class="input-field col s12 m5">
            <i class="material-icons prefix">phone</i>
            <input id="" type="text" class="validate">
            <label for="">Numero de Teléfono</label>
          </div>
        </div>
      <!--Correo-->
      <div class="row">
        <div class="col s4"><h1></h1></div>
        <div class="input-field col s12 m5">
            <a class="waves-effect waves-light btn"><i class="material-icons left">vpn_key</i>Cambiar contraseña</a>
        </div>
      </div>
      <!--Contraseña y confirmacion-->
      <div class="row">
        <div class="input-field col s12">
          <div class="col s3">
            </div>
        </div>
      </div>
      <!--Boton de registro-->
      <div class="row">
        <div class="col s5"><h1></h1></div>
        <div class="col s7">
          <a class="waves-effect waves-light btn">Guardar</a>
        </div>
      </div>
    </div>
  </div>
  <br>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems);
    });
  </script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      M.AutoInit();
    });
  </script>
@endsection
