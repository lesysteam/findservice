<?php

namespace FindService\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use FindService\Http\Requests;
use FindService\Servicio;
use FindService\ModeloUsuario;
use FindService\User;
use FindService\Http\Requests\UsuarioFormRequest;
use DB;

class PagesController extends Controller
{
    public function inicio(Request $request){
      if ($request) {
        $query=trim($request->get('searchText'));
        $servicio=DB::table('servicio')
        ->where('nombre','LIKE','%'.$query.'%')
        ->orderBy('idservicio','asc')
        ->paginate(8);
        return view('inicio',["servicio"=>$servicio,"searchText"=>$query]);
      }

      //$servicios = Servicio::get();
      //return view('inicio')->with('servicios',$servicios);
    }

    public function store(UsuarioFormRequest $request){
      $usuario=new ModeloUsuario;
      $usuario->nombre=$request->get('nombre');
      $usuario->Apellido=$request->get('apellido');
      $usuario->name=$request->get('nombre_usuario');
      $usuario->password=bcrypt($request->get('contraseña'));
      $usuario->email=$request->get('correo');
      $usuario->celular=$request->get('numero_telefono');
      $usuario->fechaNacimiento=$request->get('fecha_nacimiento');
      $usuario->save();
      return redirect('/');
    }

    public function show($id){
      return view("show",["categoria"=>ModeloCategoria::findOrFail($id)]);
    }
    public function login(){
      return view('login');
    }
    public function registerService(){
      return view('signUpService');
    }
    public function servicio(){
      return view('Servicio');
    }
    public function signup(){
      return view('signup');
    }
    public function categoriaSalud(){
      return view('CategoriaSalud');
    }

    public function blog(){
      return view('Blog');
    }

    public function verPerfil($id){
      $usuario = User::findOrFail($id);
      return view('perfil')->with('usuario',$usuario);
    }
}
