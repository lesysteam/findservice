@extends ('plantilla')

@section ('contenido')
  <!-- Contenido principal de la pagina -->
  <!--<img width="100px"src="{{ Storage::url('$servicio->logo') }}">-->
  < br><br><br><br>
  <h4 class="center-align">Registro de Servicio</h4>
  <br>

        <div class="container">
          <div class="row">
            <br><br>
            @if (count($errors)>0)
            <div class="">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{$error}}</li>
                @endforeach
              </ul>
            </div>
            @endif

            <!--<form class="col s12">-->
              {!!Form::open(array('url'=>'servicio','method'=>'POST','autocomplete'=>'off','enctype'=>'multipart/form-data'))!!}
              {{Form::token()}}
              <!--Logo-->
              <div class="row">
                <div class="col s8 offset-s2">
                  <div class="file-field input-field">
                    <div class="btn">
                      <span>Subir Imagen</span>
                      <input name="logoServicio" type="file" accept=".jpg, .png">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text">
                    </div>
                  </div>
                </div>
              </div>
              <!--Nombre del Servicio-->
              <div class="row">
                <div class="input-field col s8 offset-s2">
                  <i class="material-icons prefix">business</i>
                  <input name="nombreServicio" id="nombreServicio" type="text" class="validate" Required>
                  <label for="nombreServicio">Nombre del Servicio</label>
                </div>
              </div>
              <!--Categoria del Servicio-->
              <div class="input-field col s8 offset-s2">
                <select name="CategoriaServicio">
                  <option value="" disabled selected>Seleccione una Categoría</option>
                  <option value="1">Salud</option>
                  <option value="2">Comida</option>
                  <option value="3">Comercio</option>
                  <option value="4">Profesional</option>
                  <option value="5">Ocio</option>
                </select>
                <label style="font-size: 16px;">¿En qué Categoría considera su Servicio</label>
              </div>
              <!--Direccion del Servicio-->
              <div class="row">
                <div class="input-field col s8 offset-s2">
                  <i class="material-icons prefix">location_on</i>
                  <input name="direccion" id="register_address" type="text" class="validate">
                  <label for="register_address">Direccion del Servicio</label>
                </div>
              </div>
              <!--Sucursales-->
              <!--div class="row">
                <div class="col s6">
                  <label>
                    <input name="sucursalesServicio" type="checkbox" class="filled-in"/>
                    <span>¿Sucursales?</span>
                  </label>
                </div>
              </div-->
              <!--Horarios-->
              <div class="row">
                <div class="input-field col s4 offset-s2">
                  <i class="material-icons prefix">schedule</i>
                  <input name="horarioApertura" id="horarioApertura" type="text" class="timepicker">
                  <label for="horarioApertura">Horario de Apertura</label>
                </div>
                <div class="input-field col s4">
                  <i class="material-icons prefix">schedule</i>
                  <input name="horarioCierre" id="horarioCierre" type="text" class="timepicker">
                  <label for="horarioCierre">Horario de Cierre</label>
                </div>
              </div>

              <!--Telefonos-->
              <div class="row">
                <div class="input-field col s4 offset-s2">
                  <i class="material-icons prefix">phone</i>
                  <input name="numtelefono" id="register_phone" type="text" class="validate">
                  <label for="register_phone">Numero de Teléfono</label>
                </div>
                <div class="input-field col s4">
                  <i class="material-icons prefix">phone_android</i>
                  <input name="numcelular" id="register_cell" type="text" class="validate">
                  <label for="register_cell">Numero de Celular</label>
                </div>
              </div>


              <!--Otros telefonos-->
              <!--div class="row">
                <div class="col s6">
                  <label>
                    <input name="celularServicio" type="checkbox" class="filled-in" />
                    <span>¿Más telefonos?</span>
                  </label>
                </div>
              </div-->
              <!--Contacto-->
              <div class="row">
                <div class="input-field col s8 offset-s2">
                  <i class="material-icons prefix">person</i>
                  <input name="nombrencargado" id="nombreContacto" type="text" class="validate">
                  <label for="nombreContacto">Nombre de la Persona encargada del Servicio</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s8 offset-s2">
                  <i class="material-icons prefix">email</i>
                  <input name="correoContacto" id="correoContacto" type="text" class="validate">
                  <label for="correoContacto">Correo de la Persona encargada del Servicio</label>
                </div>
              </div>
              <!--Descripción del servicio-->
              <div class="row">
                  <div class="row">
                    <div class="input-field col s8 offset-s2">
                      <i class="material-icons prefix">description</i>
                      <textarea name="descripcionServicio" id="textarea1" class="materialize-textarea"></textarea>
                      <label for="textarea1">Descripción del Servicio</label>
                    </div>
                  </div>
              </div>
              <!--Fotografias-->
              <!--div class="row">
                <div class="input-field col s11">
                    <div class="file-field input-field">
                      <div class="btn green">Subir Fotografias
                        <input type="file" name="adjunto" accept=".gif, .jpg, .png" multiple>
                      </div>
                      <div class="file-path-wrapper">
                        <input name="fotografiasServicio" class="file-path validate" type="text" placeholder="Fotografías sobre el servicio">
                      </div>
                    </div>
                </div>
              </div-->
              <!--Formato de Pago-->
              <!--p><i class="material-icons prefix">payment</i>Formato de Pago</p>
              <div class="input-field col s3">
                <label>
                  <input type="checkbox" class="filled-in"/>
                  <span>Tarjeta de Credito</span>
                </label>
              </div>
              <div class="input-field col s3">
                <label>
                  <input type="checkbox" class="filled-in"/>
                  <span>Tarjeta de Debito</span>
                </label>
              </div>
              <div class="input-field col s3">
                <label>
                  <input type="checkbox" class="filled-in"/>
                  <span>Efectivo</span>
                </label>
              </div>
              <div class="input-field col s3">
                <label>
                  <input type="checkbox" class="filled-in"/>
                  <span>Cheque</span>
                </label>
              </div-->
          <!--Pagina Web-->
          <div class="row">
            &nbsp;
            <div class="input-field col s8 offset-s2">
              <i class="material-icons prefix">pageview</i>
              <input name="urlServicio" id="paginaWeb" type="text" class="validate">
              <label for="paginaWeb">Pagina Web del Servicio</label>
            </div>
          </div>
          <!--Redes Sociales-->
          <!--div class="row">
            <div class="input-field col s11">
              <i class="material-icons prefix">web_asset</i>
              <input id="paginaWeb" type="text" class="validate">
              <label for="paginaWeb">Link de Sus Redes Sociales</label>
            </div>
          </div-->
          <!--Otras Redes Sociales-->
          <!--div class="row">
            <div class="col s6">
              <label>
                <input type="checkbox" class="filled-in"/>
                <span>¿Otras Redes Sociales?</span>
              </label>
            </div>
          </div-->
          <!--Boton de registro-->
          <div class="row">
            <div class="col s6 offset-s3 offset-l4">

              <button type="submit" class="btn-large green" name="button">Registrarme</button>
            </div>
          </div>
          {!!Form::close()!!}
          <!--</form>-->
        </div>
      </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('.carousel');
      var instances = M.Carousel.init(elems);
    });
    var instance = M.Carousel.init({
      fullWidth: true,
      indicators: true
    });
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('select');
      var instances = M.FormSelect.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.timepicker');
      var instances = M.Timepicker.init(elems,{
        twelveHour:false
      });
    });
  </script>
@endsection
