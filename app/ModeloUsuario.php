<?php

namespace FindService;

use Illuminate\Database\Eloquent\Model;

class ModeloUsuario extends Model
{
    protected $table='users';
    protected $primaryKey="id";
    public $timestamps=false;

    //atributos que van a cambiar en la tabla de la BD
    protected $filable =[
      'nombre',
      'apellido',
      'name',
      'password',
      'email',
      'celular',
      'fechaNacimiento'
    ];

    //para campos que no queremos que cambien en la tabla de la BD
    protected $guarded=[

    ];

}
